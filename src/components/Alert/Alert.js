import React from "react";
import './Alert.css'

const alert = props =>{
    return (
        <div className={['Alert', props.type].join(', ')}>
            {props.children}
            {props.desmiss !== undefined ? <button>X</button> : null}
        </div>
    )
}
export default alert;