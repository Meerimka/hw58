import React, { Component, Fragment } from 'react';


import './App.css';
import Modal from "./components/Modal/Modal";
import Button from "./components/buttons/button"
import Alert from './components/Alert/Alert';

class App extends Component {
  state = {
    showModal: false
  }

  showModal = () => {
    this.setState({
        showModal: true
    })
  };

  closeModal = () => {
    this.setState({
        showModal: false
    })
  }
  desmiss = () =>{

  }
  render() {
    return (
      <div className="App">
        <Fragment>
        <Modal
            close={this.closeModal}
            show={this.state.showModal}
        >
            <h3>Tittle</h3>
            <p>Somthing</p>
        </Modal>
            <Button onClick={this.showModal}>Show Modal</Button>
          <Alert
            type='danger'
            desmiss = {this.desmiss}
          >
            <h3>This is danger Alert</h3>
          </Alert>

            <Alert
            type='primary'
          >
            <h3>This is primary Alert</h3>
          </Alert>

            <Alert
            type='warning'
            desmiss = {this.desmiss}
          >
            <h3>This is warning Alert</h3>
          </Alert>
            <Alert
            type='success'
          >
            <h3>This is success Alert</h3>
          </Alert>
        </Fragment>

      </div>
    );
  }
}

export default App;
